source : http://www.w3sdesign.com

The Observer design pattern solves problems like:
How can a one-to-many dependency between objects be defined without making the objects tightly coupled?
 How can an object notify  other objects?
 
 The Observer design pattern provides a solution:
 Define Subject and Observer objects so that when a subject changes state,all registered observers are notified and updated automatically.

