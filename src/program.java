public class program {
    public static void main(String[] args) {
        // créer le sujet
        Sujet sujet=new SujetImpl1();
        // creer les observer et ajouter au sujet pour etre notifiés
        Iobservateur observateur1 = new ObserveurImpl1(sujet);
        Iobservateur observateur3 = new ObserveurImpl3(sujet);
        //  // creer les observer
        Iobservateur observateur2 = new ObserveurImpl2(null);
        //souscrire
        sujet.AttacheObservateur(observateur2);
        //detcher
        sujet.DettachObservateur(observateur1);
        sujet.setEtatsujet(5);
        sujet.notifier();;

    }
}
