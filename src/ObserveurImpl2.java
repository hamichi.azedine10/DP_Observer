public class ObserveurImpl2 extends  Iobservateur {
    public ObserveurImpl2(Sujet sujet) {
        super.setSujet(sujet);
        // ajouter l'observateur au sujet au mooment de creation
        if (sujet!=null)
        sujet.AttacheObservateur(this);

    }

    @Override
    public void update() {

        System.out.println(this.getClass().getSimpleName()+" ,  l'etat de sujet a changé :"+getSujet().getEtatsujet());
    }
}
