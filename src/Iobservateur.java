public abstract class Iobservateur {
    public abstract void update();
    private    Sujet sujet;

    public Sujet getSujet() {
        return sujet;
    }

    public void setSujet(Sujet sujet) {
        this.sujet = sujet;
    }
}
