import java.util.*;

public abstract  class Sujet  {
    Set<Iobservateur> iobservateurList= new HashSet<>();
    private int etatsujet ;

    public abstract int getEtatsujet() ;
    public abstract void setEtatsujet(int etatsujet);
    public abstract void AttacheObservateur (Iobservateur iobservateur);
    public abstract void  DettachObservateur (Iobservateur iobservateur);
    public  abstract void notifier();


}
