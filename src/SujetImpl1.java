import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public   class SujetImpl1 extends  Sujet {
    Set<Iobservateur> iobservateurList= new HashSet<>();
    protected int etatsujet = 0;
    Iterator<Iobservateur> iobservateurIterator;

    public int getEtatsujet() {
        return etatsujet;
    }

    public void setEtatsujet(int etatsujet) {
        this.etatsujet = etatsujet;
    }

    public  void AttacheObservateur (Iobservateur iobservateur){
        iobservateur.setSujet(this);
       iobservateurList.add(iobservateur);
   };
   public  void  DettachObservateur (Iobservateur iobservateur){
       System.out.println(" detacher l'observateur "+iobservateur.getClass().getSimpleName());
      iobservateurList.remove(iobservateur);
   };

   public  void notifier(){
       System.out.println("le sujet a changé d'etat, notifier tous ses observateurs ");
       iobservateurIterator = iobservateurList.iterator();
       while (iobservateurIterator.hasNext()){
           //System.out.println(iobservateurIterator.next().getClass().getSimpleName());
           iobservateurIterator.next().update();
       }
   }

}
